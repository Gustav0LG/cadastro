<?php

try
{
    // Timezone default
    date_default_timezone_set("America/Sao_Paulo");
    // Iincianndo session
    session_start();
    // Pasta da raiz do projeto
    define('RAIZ', __DIR__);
    // Pasta com as class e funções usadas no projeto
    define('APP', RAIZ.'/app/');
    // Pasta com as class
    define('CLASSES', APP.'/class/');
    // Pasta com os arquivos de configurações
    define('CONFIG', RAIZ.'/config/');
    // Pasta com os recursos gráficos gerados com o php
    define('RESOURCES', RAIZ.'/resources/');
    // Arquivo de suporte com aa funções globais do sistema
    require_once(APP.'/functions.php');
    // Carregar configurações globas
    define('SISTEMA', getConfig('app'));
    // Arquivo com os Endpont(Rotas do projeto) do projetos
    require_once('routes.php');
}
catch(Exception $e)
{
    echo 'Opa.., Ocorreu um erro no servidor \n';
    echo 'Messagen de erro:', $e->getMessage(), '\m';
}
