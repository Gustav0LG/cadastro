-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 09-Jun-2020 às 02:47
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `sistema`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_cidade`
--

CREATE TABLE `sys_cidade` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_estado` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_clientes`
--

CREATE TABLE `sys_clientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `id_cidade` int(10) UNSIGNED NOT NULL,
  `address` varchar(255) NOT NULL,
  `t_update` datetime DEFAULT NULL,
  `t_delete` datetime DEFAULT NULL,
  `t_create` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_estados`
--

CREATE TABLE `sys_estados` (
  `id` int(10) UNSIGNED NOT NULL,
  `sigla` varchar(2) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_produto`
--

CREATE TABLE `sys_produto` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_user` bigint(10) UNSIGNED DEFAULT NULL,
  `t_update` datetime DEFAULT NULL,
  `t_delete` datetime DEFAULT NULL,
  `t_create` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_session`
--

CREATE TABLE `sys_session` (
  `pin` varchar(10) NOT NULL,
  `token` varchar(255) NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `t_update` datetime DEFAULT NULL,
  `t_create` datetime NOT NULL,
  `id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sys_users`
--

CREATE TABLE `sys_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(1) NOT NULL,
  `t_update` datetime DEFAULT NULL,
  `t_delete` datetime DEFAULT NULL,
  `t_create` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `sys_cidade`
--
ALTER TABLE `sys_cidade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_restrict_estados` (`id_estado`);

--
-- Índices para tabela `sys_clientes`
--
ALTER TABLE `sys_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `sys_estados`
--
ALTER TABLE `sys_estados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sigla` (`sigla`);

--
-- Índices para tabela `sys_produto`
--
ALTER TABLE `sys_produto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_restrict_user` (`id_user`);

--
-- Índices para tabela `sys_session`
--
ALTER TABLE `sys_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_restrict` (`id_user`);

--
-- Índices para tabela `sys_users`
--
ALTER TABLE `sys_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `sys_cidade`
--
ALTER TABLE `sys_cidade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_clientes`
--
ALTER TABLE `sys_clientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_estados`
--
ALTER TABLE `sys_estados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_produto`
--
ALTER TABLE `sys_produto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_session`
--
ALTER TABLE `sys_session`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sys_users`
--
ALTER TABLE `sys_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `sys_produto`
--
ALTER TABLE `sys_produto`
  ADD CONSTRAINT `id_restrict_user` FOREIGN KEY (`id_user`) REFERENCES `sys_clientes` (`id`);

--
-- Limitadores para a tabela `sys_session`
--
ALTER TABLE `sys_session`
  ADD CONSTRAINT `id_restrict` FOREIGN KEY (`id_user`) REFERENCES `sys_users` (`id`) ON DELETE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
