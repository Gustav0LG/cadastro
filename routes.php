<?php

use Classes\Supporte\Route;

Route::redirect(['GET'], SISTEMA['url'],'login');

Route::get(SISTEMA['url']."carregar", Classes\Controller\Database::class, ['auth']);

// Endpont de Login
Route::get(SISTEMA['url'].'login', Classes\Controller\Login::class, ['notAuth']);
Route::post(SISTEMA['url'].'login', Classes\Controller\Login::class, ['notAuth']);

// Endpont do Cadastro
Route::get(SISTEMA['url'].'cadastro', Classes\Controller\Cadastro::class, ['notAuth']);
Route::post(SISTEMA['url'].'cadastro', Classes\Controller\Cadastro::class, ['notAuth']);

// Endpont de Logout
Route::group(['GET', 'POST'], SISTEMA['url'].'logout', Classes\Controller\Logout::class, ['auth']);

// Endpont Cidades e Estados
Route::get(SISTEMA['url'].'cidade/estado/consulta', Classes\Controller\Cidades::class, ['auth']);
Route::group(['GET', 'POST'],SISTEMA['url'].'cidades/{id}', Classes\Controller\Cidades::class, ['auth']);
Route::group(['GET', 'POST'],SISTEMA['url'].'sistema/{option}', Classes\Controller\Cidades::class, ['auth']);

// Painel Inicial
Route::get(SISTEMA['url'].'dashboard', Classes\Controller\Dashboard::class, ['auth']);

// Endpont Cliente
Route::get(SISTEMA['url'].'clientes', Classes\Controller\Cliente::class, ['auth']);
Route::group(['GET','POST'],SISTEMA['url'].'clientes/{option}', Classes\Controller\Cliente::class, /*['auth']*/);

// Endpont produtos
Route::get(SISTEMA['url'].'produtos', Classes\Controller\Produtos::class, ['auth']);
Route::group(['GET', 'POST'],SISTEMA['url'].'produtos/{option}', Classes\Controller\Produtos::class, ['auth']);

Route::group(['GET', 'POST'], '*', function(){
    echo('404');
});
