<?php

return [
    'auth' => Classes\Middleware\Authenticator::class,
    'notAuth' => Classes\Middleware\NotAuthenticator::class,
];