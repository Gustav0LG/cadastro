<?php

return [
    'serve' => 'mysql',
    'host' => 'localhost',
    'dbname' => 'sistema',
    'user' => 'root',
    'pass' => '',
    'dns' => 'mysql:host=localhost;dbname=sistema'
];
