<?php

namespace Classes\Middleware;

use Classes\Supporte\Auth;

class Authenticator
{
    function __construct(&$next)
    {
        if (Auth::guard(true) === false)
        {
            header('Location: '.SISTEMA['url'].'login');
            http_response_code(301);
            return $next = false;
        }
    
        return $next = true;
    }
}