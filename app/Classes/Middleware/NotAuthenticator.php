<?php

namespace Classes\Middleware;

use Classes\Supporte\Auth;

class NotAuthenticator
{
    function __construct(&$next)
    {
        if (Auth::guard(true) === true)
        {
            header('Location: '.SISTEMA['url'].'dashboard');
            http_response_code(301);
            return $next = false;
        }

        return $next = true;
    }
}