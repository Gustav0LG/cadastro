<?php 

namespace Classes\Controller;

use Classes\Supporte\Auth;
use Classes\Supporte\Response;

class Logout
{
    function __construct(Response $response, array $args)
    {
        Auth::logout();

        return $response->render('layout', [
            "page" => $response->renderContainer('pages/logout', [
                'load' => $response->renderContainer('components/load')
            ])
        ]);
    }
}