<?php

namespace Classes\Controller;

use Classes\Supporte\Auth;
use Classes\Supporte\Response;

class Cidades
{
    function __construct(Response $response, array $args)
    {
        $id = (isset($args['id']))? $id = filter_var($args['id'], FILTER_VALIDATE_INT) : false;

        if ($id)
        {
            return $this->list($response, $id);
        }
        elseif (!isset($args['id']) and !isset($args['option']) )
        {
            return $this->page($response);
        }
        elseif (isset($args['option']))
        {
            return $this->relatorio();
        }

        return $response->json(['status' => 'Error', 404]);
    }

    private function relatorio()
    {

        $fileName = "relatorio_do_sistema_".date('Y_m_d_H_i_a').".csv";

        header('Content-type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$fileName);
        header('Content-Transfer-Encoding: binary');
        header('Pragma: no-cache');


        $data = $this->data();

        $out = fopen('php://output', 'w');

        foreach($data as $item)
        {
            fwrite($out, convertArray(["Estado: ".$item['name']]));
            fwrite($out, convertArray(["Sigla: ".$item['sigla']]));
            fwrite($out, convertArray(["Número de Cliente: ".$item['users']]));
            fwrite($out, convertArray([]));

            fwrite($out, convertArray([
                "Município",
                "Número de Cliente"
            ]));

            foreach($item['cidades'] as $cidade)
            {
                fwrite($out, convertArray([
                    $cidade['name'],
                    $cidade['users']
                ]));
            }

            fwrite($out, convertArray([]));

        }

        fclose($out);

        exit();
    }

    private function list(Response $response, int $id)
    {
        $conn = \connect();
        $stmt = $conn->prepare('SELECT `id`, `name` FROM `sys_cidade` WHERE `id_estado` = :id;');
        $stmt->bindValue('id', $id, \PDO::PARAM_INT);

        if ($stmt->execute())
        {
            return $response->json(['status' => 'Success', 'data' => $stmt->fetchAll()]);
        }

        return $response->json(['status' => 'Error', 'data' => ''], 506);
    }

    private function page(Response $response)
    {
        return $response->render('layout', [], [
            'auth' => true,
            'page' => $response->renderContainer('pages/cidades', ['data' => $this->data()]),
            'user' => Auth::user(),
            'bread' => [['url' => 'cidade/estado/consulta', 'title' => 'Consulta - Cidades e Estados']]
        ]);
    }

    private function data()
    {
        $data = [];
        $conn = connect();

        $estados = $conn->query('SELECT * FROM `sys_estados`;');
        $estados = $estados->fetchAll();

        $cidades = $conn->query('SELECT * FROM `sys_cidade`;');
        $cidades = $cidades->fetchAll();

        $clientes = $conn->query('SELECT * FROM `sys_clientes`;');
        $clientes = $clientes->fetchAll();

        foreach ($estados as $estado)
        {
            $data[$estado['id']] = $estado;

            $user = 0;
            $num = 0;

            foreach ($cidades as $cidade)
            {
                if ($cidade['id_estado'] == $estado['id'])
                {
                    $data[$estado['id']]['cidades'][$num] = $cidade;
                    $userc = 0;

                    foreach ($clientes as $cliente)
                    {
                        if ($cliente['id_cidade'] == $cidade['id'])
                        {
                            $userc++;
                        }
                        unset($cliente);
                    }

                    $data[$estado['id']]['cidades'][$num]['users'] = $userc;
                    unset($cidade);
                    $num++;
                }
            }

            $data[$estado['id']]['users'] = $user;

            unset($estado);
        }

        return $data;
    }
}
