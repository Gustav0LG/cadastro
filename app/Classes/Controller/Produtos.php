<?php

namespace Classes\Controller;

use Classes\Supporte\Response;
use Classes\Supporte\Auth;

class Produtos
{
    function __construct(Response $response, array $args)
    {
        if (!isset($args['id']) and !isset($args['option']))
        {
            return $this->page($response);
        }
        elseif (isset($args['option']))
        {
            $option = filter_var($args['option'], FILTER_SANITIZE_STRING);

            switch($option)
            {
                case "list":
                    return $this->list($response);
                    break;
                case "create":
                    return $this->create($response);
                    break;
                case "delete":
                    return $this->delete($response);
                    break;
                case "emprestar":
                    return $this->emprestar($response);
                    break;
                case "":
                    break;
                case "":
                    break;
            }
        }

        return $response->json(['status' => 'Error'], 404);
    }

    private function emprestar(Response $response)
    {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT); 
        $id_cliente = filter_input(INPUT_POST, 'id_cliente', FILTER_SANITIZE_NUMBER_INT); 

        if($id != null and $id > 0 and $id_cliente != null and $id_cliente > 0)
        {
            $conn = connect();
            $stmt = $conn->prepare('SELECT `id` FROM `sys_clientes` WHERE `id` = :id LIMIT 1;');
            $stmt->bindValue('id', $id_cliente, \PDO::PARAM_INT);

            if ($stmt->execute() and $stmt->rowCount() > 0)
            {
                $stmt = $conn->prepare('UPDATE `sys_produto` SET `id_user` = :id_cliente, `t_update` = :data WHERE `sys_produto`.`id` = :id');
                $stmt->bindValue('id', $id, \PDO::PARAM_INT);
                $stmt->bindValue('data', date("Y-m-d H:i:s"), \PDO::PARAM_STR);
                $stmt->bindValue('id_cliente', $id_cliente, \PDO::PARAM_INT);

                if ($stmt->execute() and $stmt->rowCount() > 0)
                {
                    return $response->json(['status' => 'Success']);
                }
            }
        }

        return $response->json(['status' => 'Error', 'errors' => ['erro' => 'Ocorreu um erro no desconhecido']], 422);
    }

    private function create(Response $response)
    {
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

        if ($name != null and strlen($name) < 100 and strlen($name) > 1)
        {
            $conn = connect();
            $stmt = $conn->prepare('INSERT INTO `sys_produto` (`id`, `name`, `id_user`, `t_update`, `t_delete`, `t_create`) VALUES (NULL, :name, NULL, NULL, NULL, :data)');
            $stmt->bindValue('name', $name, \PDO::PARAM_STR);
            $stmt->bindValue('data', date("Y-m-d H:i:s"), \PDO::PARAM_STR);

            if ($stmt->execute() and $stmt->rowCount() > 0)
            {
                return $response->json(['status' => 'Success']);
            }
        }
        elseif ($name != null and strlen($name) > 100 and strlen($name) < 1)
        {
            return $response->json(['status' => 'Error', 'errors' => ['name' => 'O campo "Nome" não é valido']], 422);
        }

        return $response->json(['status' => 'Error'], 422);
    }

    private function delete(Response $response)
    {
        $id = (isset($_POST['id']))? filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT) : 0;

        if ($id > 0)
        {
            $conn = connect();
            $stmt = $conn->prepare('DELETE FROM `sys_produto` WHERE `id` = :id LIMIT 1;');
            $stmt->bindValue('id', $id, \PDO::PARAM_INT);
    
            if ($stmt->execute() and $stmt->rowCount() > 0)
            {
                return $response->json(['status' => 'Success']);
            }
        }

        return $response->json(['status' => 'Error'], 404);
    }

    private function list(Response $response)
    {
        $conn = connect();
        $stmt = $conn->query('SELECT * FROM `sys_produto`;');

        $response->json(['status' => 'Success', 'data' => $stmt->fetchAll()]);
    }

    private function page(Response $response)
    {
        $conn = connect();
        $stmt = $conn->query('SELECT `id`, `name` FROM `sys_clientes` ');
        $clientes = $stmt->fetchAll();
        $load = $response->renderContainer('components/load');

        return $response->render('layout', [], [
            'auth' => Auth::getStatus(),
            'user' => Auth::user(),
            'page' => $response->renderContainer('pages/produtos', [
                'registre' => $response->renderContainer('components/produto_registre',[
                    'load' => $load
                ]),
                'empreste' => $response->renderContainer('components/produto_empreste', [
                    'load' => $load,
                    'clientes' => $clientes
                ])
            ]),
            'bread' => [
                ['url' => 'produtos', 'title' => 'Consulta - Produtos']
            ]
        ]);
    }
}