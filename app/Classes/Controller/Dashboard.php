<?php

namespace Classes\Controller;

use Classes\Supporte\Auth;
use Classes\Supporte\Response;


class Dashboard
{
    function __construct(Response $response, array $args)
    {
        return $response->render('layout', [], [
            'auth' => Auth::getStatus(),
            'user' => Auth::user(),
            'page' => $response->renderContainer('pages/dashboard', [])
        ]);
    }
}
