<?php

namespace Classes\Controller;

use Classes\Supporte\Auth;
use Classes\Supporte\Response;

class Login
{
    function __construct(Response $response, array $args)
    {
        if ($_SERVER['REQUEST_METHOD'] === "GET")
        {
            return $response->render('layout', [
                "page" => $response->renderContainer('pages/login', [
                    'load' => $response->renderContainer('components/load')
                ])
            ]);
        }
        elseif ($_SERVER['REQUEST_METHOD'] === "POST")
        {
            $this->login($response, $args);
        }
    }

    private function login($response, $args)
    {   
        $res = [];

        $login = filter_input(INPUT_POST, 'login', FILTER_VALIDATE_EMAIL);
        $password = filtro_basico($_POST['password'] ?? '');


        if (!isset($_POST['login']) or strlen(trim($_POST['login'])) == 0)
        {
            array_push($res, 'O campo "Login" é obrigatoria.');
        }
        elseif($login == false)
        {
            array_push($res, 'O login é invalido.');
        }
        
        if ($password === null or strlen($password) <= 0)
        {
            array_push($res, 'A campo "Senha" é obrigatoria.');
        }

        if (count($res) <= 0)
        {
            if (Auth::login($login, $password))
            {
                $response->json(['status' => 'Success'], 200);
            }
            else
            {
                $response->json(['status' => 'Erro', 'error' => 'Usuário ou/e senha invalido/s'], 422);
            }
        }
        else
        {
            $response->json(['status' => 'Erro', 'error' => 'Ocorreu um erro:', 'errors' => $res], 422);
        }
    }
}
