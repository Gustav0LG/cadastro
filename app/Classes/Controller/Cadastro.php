<?php

namespace Classes\Controller;

use Classes\Supporte\Auth;
use Classes\Supporte\Response;

class Cadastro
{
    function __construct(Response $response, array $args)
    {
        if ($_SERVER['REQUEST_METHOD'] === "GET")
        {
            return $response->render('layout', [
                "page" => $response->renderContainer('pages/cadastro', [
                    'load' => $response->renderContainer('components/load')
                ])
            ]);
        }
        elseif ($_SERVER['REQUEST_METHOD'] === "POST")
        {
            $this->cadastro($response, $args);
        }
    }

    private function cadastro($response, $args)
    {
        $res = [];
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $login = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
        $password = filtro_basico($_POST['password'] ?? '');
        $password_c = filtro_basico($_POST['password_c'] ?? '');
        $termos = (isset($_POST['termos']) and $_POST['termos'] === 'on') ? true : false;

        if (!$name)
        {
            $res['name'] = 'O campo "Nome" é obrigatorio.';
        }
        elseif (\strlen($name) < 10)
        {
            $res['name'] = 'O campo "Nome" não atingiu o minimo de 10 caracteres.';
        }
        elseif (\strlen($name) > 100)
        {
            $res['name'] = 'O campo "Nome" ultrapassou o limite de 100 caracteres.';
        }

        if ($login == false)
        {
            $res['login'] = 'O campo "Login" não é um e-mail valido.';
        }
        elseif (Auth::hasLogin($login))
        {
            $res['login'] = 'Esse E-mail já foi cadastrado - "'.$login.'".';
        }

        if ($password == null)
        {
            $res['password'] = 'O campo "Senha" é obrigatorio.';
        }
        elseif($password_c == null)
        {
            $res['password_c'] = 'O campo "Confirmação de Senha" é obrigatorio.';
        }
        elseif (\strlen($password) < 6 or \strlen($password_c) < 6)
        {
            $res['password'] = 'O campo "Senha ou Confirmações de senha" não atingiu o minimo de 6 caracteres.';
        }
        elseif($password !== $password_c)
        {
            $res['password'] = 'A "Senha" inserida é diferente da sua confirção.';
        }

        if ($termos == false)
        {
            $res['termos'] = 'Para efetuar o cadastro é necessario aceitar os termos de uso.';
        }

        if (count($res) === 0)
        {
            if (Auth::singIn(compact(['name', 'login', 'password'])))
            {
                return $response->json(['status' => 'Success', 'success' => 'Usuário cadastrado com sucesso.']);
            }
        }   

        return $response->json(['status' => 'Error', 'error' => 'Ocorreu um erro:', 'errors' => $res], 422);
    }

}
