<?php

namespace Classes\Controller;

use Classes\Supporte\Response;
use Classes\Supporte\Auth;

class Cliente
{
    function __construct(Response $response, array $args)
    {
        if ($_SERVER['REQUEST_METHOD'] === "GET" and !isset($args['option']))
        {
            return $this->page($response);
        }
        elseif (isset($args['option']))
        {
            switch($args['option'])
            {
                case "listar":
                    return $this->list($response);
                    break;
                case "delete": 
                    return $this->delete($response, $args);
                    break;
                case "show":
                    return $this->show($response, $args);
                    break;
                case "edit":
                    return $this->edit($response, $args);
                    break;
                case "relatorio":
                    return $this->relatorio();
                    break;
                case "create":
                    return $this->create($response);
                    break;
                default:
                    $response->json(['status' => 'Error'], 404);
            }
        }
    }

    private function edit(Response $response, array $args)
    {
        $res = [];
        $conn = \connect();
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_NUMBER_INT);
        $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
        $id_cidade = filter_input(INPUT_POST, 'id_cidade', FILTER_SANITIZE_NUMBER_INT);

        if ($id == null or $id < 1)
        {
            $res['id'] = 'O campo "ID" é necessario.';
        }
        elseif ($id != null and $id > 0)
        {
            $stmt = $conn->prepare('SELECT `id` FROM `sys_clientes` WHERE `id` = :id LIMIT 1;');
            $stmt->bindValue('id', $id, \PDO::PARAM_INT);

            if (!$stmt->execute() or $stmt->rowCount() < 1)
            {
                $res['id'] = 'O campo "ID" é invalido';
            }
        }

        if ($name != null and strlen($name) > 100)
        {
            $res['name'] = 'O campo "Nome" atingiu o limite de 100 caracteres.';
        }   

        if ($phone != null and strlen($phone) > 18)
        {
            $res['phone'] = 'O campo "Telefonne" atingiu o limite de 18 caracteres.';
        }

        if ($address != null and strlen($address) > 255)
        {
            $res['address'] = 'O campo "Endereço" atingiu o limite de 255 caracteres.';
        }

        if ($id_cidade != null and \is_numeric($id_cidade) and $id_cidade > 0)
        {
            $stmt = $conn->prepare('SELECT `id`FROM `sys_cidade` WHERE `id` = :id LIMIT 1;');
            $stmt->bindValue('id', $id_cidade, \PDO::PARAM_INT);
            $stmt->execute();
    
            if ($stmt->rowCount() < 0)
            {
                $res['cidade'] = 'O município não corresponde.';
            }
        }

        if (count($res) === 0)
        {
            $update = '`t_update` = "'.date("Y-m-d H:i:s").'"';

            foreach(\compact('name', 'phone', 'address', 'id_cidade') as $key => $value)
            {  
                if ($value != null)
                {
                    $update .= ($update == null) ? '' : ', ';
                    $update .= '`'.$key.'` = "'.$value.'"';
                }
            }

            $stmt = $conn->prepare('UPDATE `sys_clientes` SET '.$update.' WHERE `id` = :id');
            $stmt->bindValue('id', $id, \PDO::PARAM_INT);

            if ($stmt->execute())
            {
                return $response->json(['status' => 'Success']);
            }
        }
        else
        {
            return $response->json(['status' => 'Error', 'errors' => $res], 422);
        }

        return $response->json(['status' => 'Error'], 422);
    }

    private function relatorio()
    {
        $fileName = "relatorio_de_cliente_".date('Y_m_d_H_i_a').".csv";

        header('Content-type: text/csv; charset=utf-8');   
        header('Content-Disposition: attachment; filename='.$fileName);   
        header('Content-Transfer-Encoding: binary');
        header('Pragma: no-cache');

        $conn = \connect();
        $stmt = $conn->query('SELECT `sys_clientes`.`id`, `sys_clientes`.`name`, `sys_clientes`.`phone`, `sys_estados`.`name` AS `estado`, `sys_cidade`.`name` AS `cidade`, `sys_clientes`.`address` FROM `sys_clientes` LEFT JOIN `sys_cidade` ON `sys_clientes`.`id_cidade` = `sys_cidade`.`id` LEFT JOIN `sys_estados` ON `sys_cidade`.`id_estado` = `sys_estados`.`id`;');
        $count = $stmt->rowCount();

        $out = fopen('php://output', 'w');

        fwrite($out, convertArray(["Total de Clientes", $count]));
        fwrite($out, convertArray([]));

        fwrite($out, convertArray(["ID", "Nome", "Telefone", "Estado", "Munucipio", "Endereco"]));

        if ($count > 0)
        {
            foreach($stmt->fetchAll() as $row)
            {
                fwrite($out, convertArray($row));
            }
        }

        fclose($out);

        exit();
    }

    private function show(Response $response, array $args)
    {
        $id = (isset($_POST['id']))? filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT) : 0;

        if ($id > 0)
        {
            $conn = connect();
            $stmt = $conn->prepare('SELECT `sys_clientes`.*, `sys_cidade`.`name` AS `cidade`, `sys_estados`.`name` AS `estado`  FROM `sys_clientes` LEFT JOIN `sys_cidade` ON `sys_clientes`.`id_cidade` = `sys_cidade`.`id` LEFT JOIN `sys_estados` ON `sys_cidade`.`id_estado` = `sys_estados`.`id` WHERE `sys_clientes`.`id` = :id LIMIT 1;');
            $stmt->bindValue('id', $id, \PDO::PARAM_INT);
    
            if ($stmt->execute() and $stmt->rowCount() > 0)
            {
                return $response->json(['status' => 'Success', 'cliente' => $stmt->fetchAll()[0]]);
            }
        }

        return $response->json(['status' => 'Error'], 406);
    }

    private function create(Response $response)
    { 
        $res = [];
        $conn = \connect();
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
        $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_NUMBER_INT);
        $address = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);
        $id_cidade = filter_input(INPUT_POST, 'municipio', FILTER_SANITIZE_NUMBER_INT);

        if (!strlen($name))
        {
            $res['name'] = 'O campo "Nome" é obrigatorio.';
        }
        elseif (strlen($name) > 100)
        {
            $res['name'] = 'O campo "Nome" atingiu o limite de 100 caracteres.';
        }   

        if (strlen($phone) > 18)
        {
            $res['phone'] = 'O campo "Telefonne" atingiu o limite de 18 caracteres.';
        }

        if (strlen($address) > 255)
        {
            $res['address'] = 'O campo "Endereço" atingiu o limite de 255 caracteres.';
        }


        if (!isset($id_cidade) or !is_numeric($id_cidade) or $id_cidade < 1)
        {
            $res['id_cidade'] = 'O campo "Estado / Muincípio" é obrigatorio.';
        }
        elseif (\is_numeric($id_cidade) and $id_cidade > 0)
        {
            $stmt = $conn->prepare('SELECT `id`FROM `sys_cidade` WHERE `id` = :id LIMIT 1;');
            $stmt->bindValue('id', $id_cidade, \PDO::PARAM_INT);
            $stmt->execute();
    
            if ($stmt->rowCount() < 0)
            {
                $res['cidade'] = 'O município não corresponde.';
            }
        }

        if (count($res) === 0)
        {
            $stmt = $conn->prepare("INSERT INTO `sys_clientes` (`id`, `name`, `phone`, `id_cidade`, `address`, `t_update`, `t_delete`, `t_create`) VALUES (NULL, :name, :phone, :id_cidade, :address, NULL, NULL, '".date("Y-m-d H:i:s")."')");
            $stmt->bindValue('id_cidade', $id_cidade, \PDO::PARAM_INT);
            $stmt->bindValue('name', $name, \PDO::PARAM_STR);
            $stmt->bindValue('phone', $phone, \PDO::PARAM_STR);
            $stmt->bindValue('address', $address, \PDO::PARAM_STR);

            if ($stmt->execute())
            {
                return $response->json(['status' => 'Success']);
            }
        }
        else
        {
            return $response->json(['status' => 'Error', 'errors' => $res], 422);
        }

        return $response->json(['status' => 'Error'], 422);
    }

    private function list(Response $response)
    {
        $conn = connect();
        $stmt = $conn->query('SELECT * FROM `sys_clientes`;');
        $data = $stmt->fetchAll() ?? [];

        $response->json(['status' => 'Success', 'data' => $data]);
    }

    private function delete(Response $response)
    {
        $id = (isset($_POST['id']))? filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT) : 0;

        if ($id > 0)
        {
            $conn = connect();
            $stmt = $conn->prepare('DELETE FROM `sys_clientes` WHERE `id` = :id LIMIT 1;');
            $stmt->bindValue('id', $id, \PDO::PARAM_INT);
    
            if ($stmt->execute() and $stmt->rowCount() > 0)
            {
                return $response->json(['status' => 'Success']);
            }
        }

        return $response->json(['status' => 'Error'], 406);
    }

    private function page(Response $response)
    {
        $conn = connect();
        $estado = $conn->query('SELECT * FROM `sys_estados`;');
        $estado = $estado->fetchAll();
        $load = $response->renderContainer('components/load');

        $response->render('layout', [], [
            'auth' => Auth::getStatus(),
            'user' => Auth::user(),
            'page' => $response->renderContainer('pages/cliente', [
                'registro' => $response->renderContainer('components/cliente_registre', [
                    'estados' => $estado,
                    'load' => $load
                ]),
                'edit' => $response->renderContainer('components/cliente_edit', [
                    'estados' => $estado,
                    'load' => $load
                ]),
                'visualisar' => $response->renderContainer('components/cliente_view', [])
            ]),
            'bread' => [
                ['url' => 'clientes', 'title' => 'Consulta - Clientes']
            ]
        ]);
    }
}
