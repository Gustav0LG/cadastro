<?php

namespace Classes\Controller;

use Classes\Supporte\Response;

class Database
{    
    function __construct(Response $response, array $args)
    {
        $conn = connect();

        $conn->query('TRUNCATE TABLE `sys_cidade`');
        $conn->query('TRUNCATE TABLE `sys_estados`');

        $estados = RAIZ.'/sql/dados/Estados.json';
        $estados = file_get_contents($estados);
        $estados = json_decode($estados);
        
        $conn->beginTransaction();
        
        foreach($estados as $estado)
        {
            $conn->exec("INSERT INTO `sys_estados` (`id`, `sigla`, `name`) VALUES (NULL, '".$estado->Sigla."', '".$estado->Nome."');");
        }

        $conn->commit();

        $cidades = RAIZ.'/sql/dados/Cidades.json';
        $cidades = file_get_contents($cidades);
        $cidades = json_decode($cidades);

        $conn->beginTransaction();
        
        foreach($cidades as $cidade)
        {
            $conn->exec("INSERT INTO `sys_cidade` (`id`, `id_estado`, `name`) VALUES (NULL, '".$cidade->Estado."', '".$cidade->Nome."');");
        }

        $conn->commit();

        echo ('Os dados Foram carregdos');

    }
}