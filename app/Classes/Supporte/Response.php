<?php

namespace Classes\Supporte;

class Response
{   
    public $defaultTemplateLocation = RESOURCES;

    public static function redirect($uri = '')
    {
        header('Location: '.SISTEMA['url'].$uri);
        http_response_code(301);
    }

    public function json($array, $code = 200)
    {
        header("Content-type: application/json;");
        http_response_code($code);
        echo (json_encode($array));
        return;
    }

    public function render($fileName, $contents = [], $args = [])
    {
        header("Content-type: text/html;");

        extract($args);
        extract($contents);

        ob_start();
            require_once $this->defaultTemplateLocation.$fileName.'.render.php';
            $content = ob_get_contents();
        ob_end_clean();
    
        echo $content;
    }

    public function renderContainer($fileName, $args = [])
    {
        extract($args);
        
        ob_start();
            require_once $this->defaultTemplateLocation.$fileName.'.render.php';
            $contents = ob_get_contents();
        ob_end_clean();

        return $contents;
    }
}
