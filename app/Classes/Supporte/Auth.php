<?php 

namespace Classes\Supporte;

/**
 * Auth
 */
class Auth 
{
    private static $session_name = "dc86ce59b908fbaaa36460246af4c408";
    private static $conn = null;
    private static $dados = [];
    private static $status = false;
    private static $session = [];
        
    /**
     * singIn
     *
     * @param  array $data Array contendo os dado do usuáiro
     * @return bool
     */
    public static function singIn(array $data): bool
    {
        if (self::$status === false and count($data) === 3)
        {
            $data['password'] = \password_hash($data['password'], PASSWORD_DEFAULT);

            $stmt = self::$conn->prepare("INSERT INTO `sys_users` (`id`, `name`, `email`, `password`, `role`, `t_update`, `t_delete`, `t_create`) VALUES (NULL, :name, :login, :password, 4, NULL, NULL, '".date("Y-m-d H:i:s")."');");
            $stmt->bindValue('name', $data['name'], \PDO::PARAM_STR);
            $stmt->bindValue('login', $data['login'], \PDO::PARAM_STR);
            $stmt->bindValue('password', $data['password'], \PDO::PARAM_STR);
            
            return $stmt->execute();
        }

        return false;
    }

    public static function hasLogin(string $login): bool
    {   
        self::$conn = connect();
        
        $stmt = self::$conn->prepare('SELECT `id` FROM `sys_users` WHERE `email` = :login LIMIT 1');
        $stmt->bindValue('login', $login, \PDO::PARAM_STR);

        return ($stmt->rowCount() === 1)? true : false;
    }
    
    /**
     * guard
     *
     * @param  mixed $force
     * @return bool
     */
    public static function guard(bool $force = false): bool
    {
        if ($force == true or self::$status == false)
        {
            self::$session = $_SESSION[self::$session_name] ?? null;

            if (self::$session == null)
            {
                $_SESSION[self::$session_name] = null;
                return false;
            }

            if (count(self::$session) !== 2)
            {
                $_SESSION[self::$session_name] = null;
                return false;     
            }

            if (!array_key_exists('pin', self::$session) or !array_key_exists('token', self::$session))
            {
                $_SESSION[self::$session_name] = null;
                return false;
            }

            self::$conn = connect();
            $stmt = self::$conn->prepare('SELECT `id_user` FROM `sys_session` WHERE `pin` = :pin AND `token` = :token LIMIT 1;');
            $stmt->bindValue('pin', self::$session['pin'], \PDO::PARAM_STR);
            $stmt->bindValue('token', self::$session['token'], \PDO::PARAM_STR);
            $stmt->execute();

            $id = $stmt->fetchAll()[0]['id_user'] ?? false;

            if ($id !== false)
            {
                self::getDadosUser($id);
                self::$status = true;
                return true;
            }
        }

        self::$status = false;
        return self::$status;
    }
    
    /**
     * getDadosUser
     *
     * @param  mixed $id
     * @return void
     */
    private static function getDadosUser($id): void
    {
        $stmt = self::$conn->query('SELECT `id`, `name`, `email`, `role` FROM `sys_users` WHERE `id` = '.$id.' LIMIT 1;');
        self::$dados = $stmt->fetchAll()[0];
    }
    
    /**
     * logout
     *
     * @return void
     */
    public static function logout(): void
    {
        self::$conn->query('DELETE FROM `sys_session` WHERE `pin` = '.self::$session['pin'].' AND `token` = '.self::$session['pin'].' LIMIT 1;'); 
        
        \session_unset();
        \session_destroy();
    }
    
    /**
     * login
     *
     * @param  mixed $login
     * @param  mixed $password
     * @return bool
     */
    public static function login($login, $password): bool
    {   
        \sleep(3);
        self::$conn = connect();
        $stmt = self::$conn->prepare('SELECT `id`, `email`, `password` FROM `sys_users` WHERE `email` = :dat LIMIT 1');
        $stmt->bindValue('dat', $login, \PDO::PARAM_STR);
        $stmt->execute();

        self::$dados = $stmt->fetchAll()[0] ?? false;

        if (self::$dados !== false)
        {
            if (self::validationLogin($login) and self::validationPassword($password))
            {
                return self::createSession();
            }
        }

        return false;
    }
    
    /**
     * validationLogin
     *
     * @param  mixed $login
     * @return bool
     */
    private static function validationLogin($login): bool
    {
        if (self::$dados['email'] === $login)
        { 
            return true;
        }
        return false;
    }
    
    /**
     * validationPassword
     *
     * @param  mixed $password
     * @return bool
     */
    private static function validationPassword($password): bool
    {
        return password_verify($password, self::$dados['password']);
    }
    
    /**
     * createSession
     *
     * @return bool
     */
    private static function createSession(): bool
    {
        $token = self::randToken();
        $pin = self::randPin();

        $stmt = self::$conn->prepare("INSERT INTO `sys_session` (`pin`, `token`, `id_user`, `t_update`, `t_create`) VALUES (:pin, :token, :id, NULL, :t_create)");
        $stmt->bindValue('id', self::$dados['id'], \PDO::PARAM_INT);
        $stmt->bindValue('pin', $pin, \PDO::PARAM_STR);
        $stmt->bindValue('token', $token, \PDO::PARAM_STR);
        $stmt->bindValue('t_create', date("Y-m-d H:i:s"), \PDO::PARAM_STR);

        if ($stmt->execute())
        {
            $_SESSION[self::$session_name] = [
                'pin' => $pin,
                'token' => $token
            ];

            return true;
        }

        return false;
    }
    
    /**
     * randPin
     *
     * @return int
     */
    private static function randPin(): int
    {
        return rand(0, 999999999);
    }
    
    /**
     * randToken
     *
     * @return string
     */
    private static function randToken(): string
    {
        return sha1(md5(rand().date("Y-m-d H:i:s")));
    }
    
    /**
     * user
     *
     * @return array
     */
    public static function user(): array
    {
        return self::$dados;
    }
    
    /**
     * getStatus
     *
     * @return bool
     */
    public static function getStatus(): bool
    {
        return self::$status;
    }
}