<?php

namespace Classes\Supporte;

use Classes\Supporte\Response;

class Route
{
    private static $status = false;
    private static $agrs = [];

    public static function get($uri, $callback, $middleware = [])
    {
        return self::request('GET', $uri, $callback, $middleware);
    }

    public static function post($uri, $callback, $middleware = [])
    {
        return self::request('POST', $uri, $callback, $middleware);
    }

    public static function put($uri, $callback, $middleware = [])
    {
        return self::request("PUT", $uri, $callback, $middleware);
    }

    public static function delete($uri, $callback, $middleware = [])
    {
        return self::request('DELETE', $uri, $callback, $middleware);
    }

    public static function redirect($methods, $uri, $urlRedirect)
    {
        return self::group($methods, $uri, function() use ($urlRedirect){
            header('Location: '.SISTEMA['url'].$urlRedirect);
            \http_response_code(301);
        });
    }

    public static function group($methods, $uri, $callback, $middleware = [])
    {
        return self::request($methods, $uri, $callback, $middleware);
    }

    private static function request($method, $uri, $callback, $middleware)
    {    
        if(self::validationRequest($method, $uri) or $uri == '*' and self::$status == false)
        {
            $middlewares = getConfig('middleware');

            foreach($middleware as $index)
            {
                if (array_key_exists($index, $middlewares))
                {
                    new $middlewares[$index]($next);
                    if ($next !== true)
                    {
                        return false;
                    }
                }
                else
                {
                    throw new Exception('Nenhum middleware for encontrado'); 
                    return false;
                }
            }

            self::$status = true;
            return (\is_callable($callback)) ? $callback(new Response(), self::$agrs) : new $callback(new Response(), self::$agrs);
        }

        return false;
    }

    private static function validationRequest($method, $uri)
    {
        if (is_string($method))
        {
            if (self::is_method($method) && self::is_uri($uri))
            {
                return true;
            }
        }
        elseif (is_array($method))
        {
            foreach ($method as $index)
            {
                if (self::is_method(strtoupper($index)) && self::is_uri($uri))
                {
                    return true;
                }
            }
        }

        return false;
    }

    private static function is_uri($uri) : bool
    {
        $requestUri = explode("?", $_SERVER['REQUEST_URI'])[0];
        $requestUri = explode("/", $requestUri);
        $uri = explode("/", $uri);

        if (count($uri) === count($requestUri))
        {  
            $agrs = [];

            for ($num = 0; $num < count($uri); $num++)
            {
                $agr = null; 

                if (!preg_match('/^{[a-z]*}$/', $uri[$num], $agr))
                {
                    if ($uri[$num] != $requestUri[$num])
                    {
                        return false;
                    }
                }
                else
                {
                    $agrs[preg_replace("/[^a-z]/", "", $uri[$num])] = $requestUri[$num];
                }
            }

            self::$agrs = $agrs;
            return true;
        }
        
        return false;
    }

    private static function is_method($method) : bool
    {
        if ($method === $_SERVER['REQUEST_METHOD'])
        {
            return true;
        }

        return false;
    }

}
