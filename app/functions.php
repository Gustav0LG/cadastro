<?php

/**
 * Configuração do autoload de classes do projeto
 */
spl_autoload_register(function ($class_name) {
    require_once APP.$class_name.'.php';
});

/**
 * Retorna os dados dos arquivos de configrações em <PROJET>/<CONFIG>
 * 
 * @param  string $configFileName
 * @return array
*/
function getConfig(string $configFileName)
{
    if (file_exists(CONFIG.$configFileName.'.php'))
    {
        return require(CONFIG.$configFileName.'.php');
    }   

    return [];
}

/**
 * filtro_basico
 *
 * @param  string $x
 * @return void
 */
function filtro_basico(string $x)
{
    $x = htmlspecialchars($x);
    $x = trim($x);
    $x = addslashes($x);
    return $x;
}

/**
 * connect
 *
 * @param  mixed $config
 * @return PDO
 */
function connect(string $config = null): PDO
{
    try {
        $config = ($config == null)? getConfig('database') : $config;
        $pdo = new PDO($config['dns'], $config['user'], $config['pass']);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Vai ser o default no PHP 8
        return $pdo;
    }
    catch(PDOException $e)
    {
        echo "Erro de connexão com o banco de dados \n" ;
        echo "Erro:".$e->getMessage();
    }
}

/**
 * convertArray
 *
 * @param  array $array
 * @return string
 */
function convertArray(array $array = []): string
{
    $string = "";

    foreach($array as $index)
    {
        $string .= '"'.$index.'";';
    }

    return \utf8_decode($string .= "\r\n");
}
