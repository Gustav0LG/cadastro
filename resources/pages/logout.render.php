<section class="background">
    <div class="form z-depth-2">
        <div id="loading" class="center-align">
            <?= $load ?>
        </div>
        <br>
        <h4>Logout</h4>
    </div>
    <div class="z-depth-2 boll" style="top: 20px; right: 100px;"></div>
    <div class="z-depth-2 boll" style="top: 220px; right: 300px;"></div>
    <div class="z-depth-2 boll" style="top: 420px; right: 100px;"></div>
    <div class="z-depth-2 boll" style="top: 620px; right: 300px;"></div>
</section>

<script>
    $(function(){
        setTimeout(() => {
            window.location.href = '<?= SISTEMA["url"]?>';
        }, (3000));
    });
</script>