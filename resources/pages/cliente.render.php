<section class="container">
    <div>
        <div style="padding: 15px 0;">
            <a href="#formularioRegistro" class="modal-trigger waves-effect waves-light btn tooltipped" data-position="top" data-tooltip="Registrar Novo Cliente">
                <i class="material-icons left">add</i>
                Registrar
            </a>
            <a href="<?= SISTEMA['url']?>clientes/relatorio" target="_blank" class="waves-effect waves-light btn tooltipped" data-position="top" data-tooltip="Download">
                <i class="material-icons left">cloud_download</i>
                Download
            </a>
        </div>
        <form id="search" onsubmit="return false;">
            <h5>Pesquisa</h5>
            <div class="row" style="margin: 0px; padding-top: 10px;">
                <div class="input-field col s9">
                    <input autocomplete="off" id="name" name="name" type="text">
                    <label for="name">Nome</label>
                </div>
                <div class="col s3 center-align">
                    <br>
                    <button type="submit" class="waves-effect waves-light btn tooltipped" data-position="top" data-tooltip="Pesquisar">
                        <i class="material-icons left">search</i>
                        Pesquisar
                    </button>
                </div>
            </div>
        </form>
        <div class="card" style="padding: 1em 1.5em;">
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="clientes"></tbody>
            </table>
        </div>
        <div>
            <?= $registro ?>
            <?= $visualisar ?>
            <?= $edit ?>
        </div>
    </div>
    <script>

        var clientes = [];
        var view = {};

        $(function(){
            loadCliente();
            $('.modal').modal();
            $('select').formSelect();
            $('.tooltipped').tooltip();
            $('#name').on('keyup', search);
            $('#search').on('submit', function(){
                $('#name').trigger('keyup');
            });
        });



        function search(event)
        {
            let value = $(this).val().trim().toLocaleUpperCase();

            if (value != '')
            {
                addCard(clientes.filter(function(item){
                    let name = item.name.toLocaleUpperCase();

                    if(value.length <= name.length)
                    {
                        for(let num in value)
                        {
                            if (value[num] != name[num])
                            {
                                return false;
                            }
                        }
                        
                        return item;
                    }
                }));
            }
            else
            {
                addCard();
            }
        }

        function loadCliente()
        {
            $.ajax({
                url: '<?= SISTEMA["url"]; ?>clientes/listar',
                method: 'POST',
                success: function(res)
                {
                    clientes = res.data;
                },
                error: function(error)
                {
                    console.log(error);
                },
                complete: function()
                {
                    addCard();
                }
            });
        }
        
        function editar(id)
        {
            let data = clientes.find((age) => age.id == id);

            $('#formularioEdit').modal('open');

            $('#id_edit').val(data.id);
            $('#name-edit').val(data.name);
            $('#phone-edit').val(data.phone);
            $('#address-edit').val(data.address);
        }

        function addCard(cliente = clientes)
        {
            $('#clientes').html('');

            for(let item in cliente)
            {
                $('#clientes').append(`
                    <tr data-id-client="${cliente[item].id}">
                        <td>${cliente[item].id}</td>
                        <td>${cliente[item].name}</td>
                        <td>${cliente[item].phone}</td>
                        <td class="right-align">
                            <button onClick="visualisar(${cliente[item].id})" data-position="top" data-tooltip="Visualizar" class="tooltipped waves-effect waves-light btn center-align">
                                 <i class="material-icons">visibility</i>
                            </button>
                            <button onClick="editar(${cliente[item].id})" data-position="top" data-tooltip="Editar" class="tooltipped waves-effect waves-light btn center-align">
                                <i class="material-icons">create</i>
                            </button>
                            <button onClick="deletar('${cliente[item].id}', '${cliente[item].name}')" data-position="top" data-tooltip="Deletar" style="background: #E82207;" class="tooltipped waves-effect waves-light btn center-align">
                                <i class="material-icons">delete</i>
                            </button>
                        </td>
                    </tr>
                `);
            }

            $('.tooltipped').tooltip();
        }

        function deletar(id, name)
        {

            let c = confirm('Você deseja deletar o cliente: '+name);
            
            if (c === true)
            {
                $.ajax({
                    url: '<?= SISTEMA["url"]; ?>clientes/delete',
                    method: 'POST',
                    data: {id: id},
                    success: function()
                    {
                        alert('Cliente deletado com sucesso');
                    },
                    error: function()
                    {
                        alert('Ocorreu um erro na tentativa de deletar o cliente, tente novamente');
                    },
                    complete: function()
                    {
                        loadCliente();
                    }
                });
            }
        }

        function visualisar(id)
        {
            $.ajax({
                url: '<?= SISTEMA["url"]; ?>clientes/show',
                method: 'POST',
                data: {id: id},
                success: function(res)
                {
                    view = res.cliente;
                    $('#view').html(`
                        <p><b>ID:</b> ${view['id']}</p>
                        <p><b>NOME:</b> ${view['name']}</p>
                        <p><b>TELEFONE:</b> ${view['phone']}</p>
                        <br>
                        <h6><b>Endereço</b> </h6>
                        <p><b>Estado:</b> ${view['estado']}</p>
                        <p><b>Município:</b> ${view['cidade']}</p>
                        <p><b>Endereço:</b> ${view['address']}</p>
                    `);
                },
                error: function(error)
                {
                    console.log(error);
                },
                complete: function()
                {
                    $('#formularioView').modal('open');
                }
            });
        }

    </script>
</section>