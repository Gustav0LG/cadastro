<section class="container">
    <a style="margin: 1.5em 0;" href="<?= SISTEMA['url']?>sistema/relatorio" target="_blank" class="waves-effect waves-light btn tooltipped" data-position="top" data-tooltip="Download">
        <i class="material-icons left">cloud_download</i>
        Download
    </a>
    <div class="row">
        <div class="col hide-on-small-only m3 2 lateral" >
            <h5>Estados:</h5>
             <ul class="section table-of-contents">
                <?php 
                    foreach ($data as $item)
                    {
                        echo('<li><a href="#'.$item['sigla'].'">'.$item['name'].'</a></li>');
                    }
                ?>
            </ul>
        </div>
        <div class="col s12 m9 10">
        <?php
        
        foreach ($data as $item)
        {
            echo('<div class="card scrollspy" id="'.$item['sigla'].'" style="padding: 0.8em 1.5em;">');

            echo ('<p>Estado: '.$item['name'].' - '.$item['sigla'].'<br>');
            echo ('Total de Municípios: '.count($item['cidades']).'<br>');
            echo ('Total de Clientes: '.$item['users'].'<br></p>');
            echo ('<table>
                    <thead>
                        <tr>
                            <td>Número</td>
                            <td>Município</td>
                            <td>Clientes</td>
                        </tr>
                    </thead>
            ');

            echo('<tbody>');
            
            $num = 1;
            foreach ($item['cidades'] as $cidade)
            {
                echo ('
                    <tr>
                        <td>'.$num.'</td>
                        <td>'.$cidade['name'].'</td>
                        <td>'.$cidade['users'].'</td>
                    </tr>
                ');
                $num++;
            }

            echo('</tbody></table>');

            echo('</div>');
            echo('<hr>');
        }
        ?>
        </div>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.scrollspy');
            var instances = M.ScrollSpy.init(elems, {});
        });

        $(function(){
            $('.tooltipped').tooltip();
        });

    </script>
</section>