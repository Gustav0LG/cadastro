<section class="container">
    <div>
        <div style="padding: 15px 0;">
            <button onclick="registre()" class="waves-effect waves-light btn tooltipped" data-position="top" data-tooltip="Registrar Novo Produtos">
                <i class="material-icons left">add</i>
                Registrar
            </button>
        </div>
        <form id="search" onsubmit="return false;">
            <h5>Pesquisa</h5>
            <div class="row" style="margin: 0px; padding-top: 10px;">
                <div class="input-field col s9">
                    <input autocomplete="off" id="name" name="name" type="text">
                    <label for="name">Nome</label>
                </div>
                <div class="col s3 center-align">
                    <br>
                    <button type="submit" class="waves-effect waves-light btn tooltipped" data-position="top" data-tooltip="Pesquisar">
                        <i class="material-icons left">search</i>
                        Pesquisar
                    </button>
                </div>
            </div>
        </form>
        <div class="card" style="padding: 1em 1.5em;">
            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="produtos"></tbody>
            </table>
        </div>
        <div>
            <?= $registre ?>
            <?= $empreste ?>
        </div>
    </div>
    <script>

        var produtos = [];

        $(function(){
            $('.tooltipped').tooltip();
            $('.modal').modal();
            $('select').formSelect();
            $('#name').on('keyup', search);
            loadProdutos();
        });

        function registre()
        {
            $('#formularioCreate').modal('open');
        }

        function empreste(id)
        {
            $('#id_produto').val(id);
            $('#formularioEmpreste').modal('open');
        }

        function search(event)
        {
            let value = $(this).val().trim().toLocaleUpperCase();

            if (value != '')
            {
                addTabela(produtos.filter(function(item){
                    let name = item.name.toLocaleUpperCase();

                    if(value.length <= name.length)
                    {
                        for(let num in value)
                        {
                            if (value[num] != name[num])
                            {
                                return false;
                            }
                        }
                        
                        return item;
                    }
                }));
            }
            else
            {
                addTabela();
            }
        }

        function loadProdutos()
        {
            $.ajax({
                url: '<?= SISTEMA["url"]?>produtos/list',
                method: 'POST',
                success: function(data)
                {
                    produtos = data.data;
                },
                error: function(erro)
                {
                    produtos = [];
                },
                complete: function()
                {
                    addTabela();
                }
            });
        }

        function addTabela(produto = produtos)
        {
            $('#produtos').html('');

            for(let num in produto)
            {
                $('#produtos').append(`
                    <tr>
                        <td>${produto[num].id}</td>
                        <td>${produto[num].name}</td>
                        <td>${(produto[num].id_user == null)? 'Livre' : 'Emprestado' }</td>
                        <td class="right-align">
                            <button onClick="empreste(${produto[num].id})" data-position="top" data-tooltip="Emprestar" class="tooltipped waves-effect waves-light btn center-align">
                                <i class="material-icons">autorenew</i>
                            </button>
                            <button onClick="deletar('${produto[num].id}', '${produto[num].name}')" data-position="top" data-tooltip="Deletar" style="background: #E82207;" class="tooltipped waves-effect waves-light btn center-align">
                                <i class="material-icons">delete</i>
                            </button>
                        </td>
                    </tr>
                `);
            }
        }

        function deletar(id, name)
        {
            let c = confirm('Você deseja deletar o cliente: '+name);
            
            if (c === true)
            {
                $.ajax({
                    url: '<?= SISTEMA["url"]; ?>produtos/delete',
                    method: 'POST',
                    data: {id: id},
                    success: function()
                    {
                        alert('Produto deletado com sucesso');
                    },
                    error: function()
                    {
                        alert('Ocorreu um erro na tentativa de deletar o produto, tente novamente');
                    },
                    complete: function()
                    {
                        loadProdutos();
                    }
                });
            }
        }

    </script>
</section>