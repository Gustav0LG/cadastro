<section class="background">
    <div class="form z-depth-2" >
        <div id="back" style="padding-bottom: 1em;">
            <a href="<?= SISTEMA['url'] ?>login">
                <i class="material-icons left">arrow_back_ios</i>
                Voltar
            </a>
        </div>
        <div id="loading" class="center-align" style="display: none;">
            <?= $load ?>
        </div>
        <br>
        <h4>Cadastro</h4>
        <form id="formLogin" onsubmit="return false;">
            <div id="alert"></div>
            <div class="input-field" style="margin-top: 2em;">
                <input id="name" name="name" type="text" data-length="100">
                <label for="name">Nome:</label>
            </div>
            <div class="input-field" style="margin-top: 2em;">
                <input id="login" name="email" type="text" data-length="100">
                <label for="login">E-mail:</label>
            </div>
            <br>
            <div class="row" style="padding: 0px;">
                <div class="input-field col s6" style="padding: 0px 5px 0px 0px;">
                    <input id="password" name="password" type="password">
                    <label for="password">Senha:</label>
                </div>
                <div class="input-field col s6" style="padding: 0px 0px 0px 5px;">
                    <input id="password_c" name="password_c" type="password">
                    <label for="password_c">Confimação:</label>
                </div>
            </div>
            <div style="padding: 0 1em 2em 1em;">
                <label>
                    <input name="termos" type="checkbox" />
                    <span>Aceito os termos e condições de uso.</span>
                </label>
            </div>
            <div class="right-align">
                <button type="reset" class="waves-effect waves-light btn red">
                    <i class="material-icons left">delete</i>
                    Limpar
                </button>
                <button type="submit" class="waves-effect waves-light btn">
                    <i class="material-icons left">done</i>
                    Cadastrar
                </button>
            </div>
        </form>
    </div>
    <div class="z-depth-2 boll" style="top: 20px; right: 100px;"></div>
    <div class="z-depth-2 boll" style="top: 220px; right: 300px;"></div>
    <div class="z-depth-2 boll" style="top: 420px; right: 100px;"></div>
    <div class="z-depth-2 boll" style="top: 620px; right: 300px;"></div>
</section>
<script>
    $(function(){
        $('input[type="text"]').characterCounter();
        
        $('#formLogin').on('submit', function(event){
            event.preventDefault();

            let data = $(this).serializeArray();

            $(this).hide('slow');
            $('#loading').show('slow');
            $('#alert').removeClass('alert danger');
            $('#alert').html('');
            $('#back').hide();

            $.ajax({
                url: '',
                data: data,
                method: 'POST',
                success: (data) =>
                {
                    window.location.href = `<?= SISTEMA['url']?>`;
                },
                error: (data) => setTimeout(() => 
                {
                    if (data.status == 422)
                    {   
                        $('#alert').addClass('alert danger');

                        $('#alert').append(`<div><b>${data.responseJSON.error}</b></div>`);
                        
                        if (data.responseJSON.errors)
                        {
                            for(let index in data.responseJSON.errors)
                            {
                                $('#alert').append(`<div>${data.responseJSON.errors[index]}</div>`);
                            }
                        }
                    }
                    else
                    {
                        $('#alert').addClass('alert danger');
                        $('#alert').html('Ocorreu um erro no servidor, tente novamente mais tarde.');
                    }
                }, 2000),
                complete: () => setTimeout(() => {
                    $(this).show('slow');
                    $('#loading').hide('slow');
                    $('#back').show();
                }, 2000)
            });
        });
    });
</script>