<section class="background">
    <div class="form z-depth-2">
        <div id="loading" class="center-align" style="display: none;">
            <?= $load ?>
        </div>
        <br>
        <h4>Login</h4>
        <form id="formLogin" onsubmit="return false;">
            <div id="alert"></div>
            <div class="input-field" style="margin-top: 2em;">
                <input autocomplete="username" name="login" id="login" type="text" required>
                <label for="login">E-mail:</label>
            </div>
            <div class="input-field">
                <input autocomplete="current-password" name="password" id="password" type="password" required>
                <label for="password">Senha:</label>
            </div>
            <div style="padding: 0 1em 2em 1em;">
                <label>
                    <input type="checkbox" />
                    <span>Lembrar-me</span>
                </label>
            </div>
            <div class="right-align">
                <button class="waves-effect waves-light btn">
                    <i class="material-icons left">login</i>
                    Entrar
                </button>
            </div>
            <div class="center-align" style="padding-top: 2em;">
                <p><a href="<?= SISTEMA['url'] ?>cadastro">Não possui uma conta ainda?</a></p>
            </div>
        <form>
    </div>
    <div class="z-depth-2 boll" style="top: 20px; right: 100px;"></div>
    <div class="z-depth-2 boll" style="top: 220px; right: 300px;"></div>
    <div class="z-depth-2 boll" style="top: 420px; right: 100px;"></div>
    <div class="z-depth-2 boll" style="top: 620px; right: 300px;"></div>
</section>
<script>
    $(function(){
        $('#formLogin').on('submit', function(event){
            event.preventDefault();

            let data = $(this).serializeArray();

            $(this).hide('slow');
            $('#loading').show('slow');
            $('#alert').removeClass('alert danger');
            $('#alert').html('');

            $.ajax({
                url: '',
                data: data,
                method: 'POST',
                success: (data) =>
                {
                    window.location.href = `<?= SISTEMA['url']?>`;
                },
                error: (data) => setTimeout(() => 
                {
                    if (data.status == 422)
                    {   
                        $('#alert').addClass('alert danger');

                        $('#alert').append(`<div><b>${data.responseJSON.error}</b></div>`);
                        
                        if (data.responseJSON.errors)
                        {
                            for(let index in data.responseJSON.errors)
                            {
                                $('#alert').append(`<div>${data.responseJSON.errors[index]}</div>`);
                            }
                        }
                    }
                    else
                    {
                        $('#alert').addClass('alert danger');
                        $('#alert').html('Ocorreu um erro no servidor, tente novamente mais tarde.');
                    }
                }, 2000),
                complete: () => setTimeout(() => {
                    $(this).show('slow');
                    $('#loading').hide('slow');
                }, 2000)
            });
        });
    });
</script>