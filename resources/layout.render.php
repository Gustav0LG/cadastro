<!DOCTYPE html>
<html lang="pt_BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title><?= SISTEMA['name'] ?></title>
        <link rel="stylesheet" href="<?= SISTEMA['url'] ?>assets/css/materialize.min.css">
        <link rel="stylesheet" href="<?= SISTEMA['url'] ?>assets/css/style.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="<?= SISTEMA['url'] ?>assets/js/materialize.min.js"></script>
    </head>
    <body>
        <main id="app">
            <?php if(isset($auth) && $auth === true): ?>
                <div class="fixed-menu">
                    <nav>
                        <div class="nav-wrapper color">
                            <a href="#" style="padding-right: 0.4em;" class="brand-logo right"><?= SISTEMA['name'] ?></a>
                            <ul id="nav-mobile" class="left">
                                <li>
                                    <a href="javascript:void(0)" style="display: block; margin: 0;" data-target="slide-out" class="sidenav-trigger" ><i class="material-icons light">menu</i></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <nav style="padding: 0 1em; background: rgba(0, 0, 0, 0.6)!important;">
                        <div class="nav-wrapper">
                            <div class="col s12">
                                <a href="<?= SISTEMA['url'] ?>" class="breadcrumb">Dashboard</a>
                                <?php 
                                    if (isset($bread))
                                    {
                                        foreach($bread as $breadcrumb)
                                        {
                                            echo('<a href="'.SISTEMA['url'].$breadcrumb['url'].'" class="breadcrumb">'.$breadcrumb['title'].'</a>');
                                        }
                                    }    
                                ?>
                            </div>
                        </div>
                    </nav>
                </div>
                <ul id="slide-out" class="sidenav">
                    <li class="card_img_perfil">
                        <div class="card_img_perfil z-depth-2">
                            <span><?= $user['name'][0] ?></span>
                        </div>
                    </li>
                    <li>
                        <p class="center-align"><?= $user['name'] ?></p>
                        <hr>
                    </li>
                    <li>
                        <ul class="collapsible">
                            <li>
                                <a href="<?= SISTEMA['url'] ?>" style="padding: 0px;">
                                    <div class="collapsible-header">
                                        <i class="material-icons">home</i>
                                        Dashboard
                                    </div>
                                    <div class="collapsible-body"></div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= SISTEMA['url'] ?>clientes" style="padding: 0px;">
                                    <div class="collapsible-header">
                                        <i class="material-icons light">group</i>
                                        Clientes
                                    </div>
                                    <div class="collapsible-body"></div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= SISTEMA['url'] ?>produtos" style="padding: 0px;">
                                    <div class="collapsible-header">
                                        <i class="material-icons">shopping_bag</i>
                                        Produtos
                                    </div>
                                    <div class="collapsible-body"></div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= SISTEMA['url']?>cidade/estado/consulta" style="padding: 0px;">
                                    <div class="collapsible-header">
                                        <i class="material-icons light">apartment</i>
                                        Cidades & Estados
                                    </div>
                                    <div class="collapsible-body"></div>
                                </a>
                            </li>
                            <li>
                                <a href="<?= SISTEMA['url'] ?>logout" style="padding: 0px;">
                                    <div class="collapsible-header danger">
                                        <i class="material-icons left">logout</i>
                                        Sair
                                    </div>
                                    <div class="collapsible-body"></div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div style="height: 138px; display: block;"></div>
            <?php endif;?>
            <?php echo((isset($page)) ? $page : ''); ?>
        </main>
        <script>
            $(document).ready(function(){
                $('.sidenav').sidenav();
                $('.collapsible').collapsible();
            });
        </script>
    </body>
</html>