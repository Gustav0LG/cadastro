<form onsubmit="return false;" id="formularioEdit" style="max-height: 90%; overflow: inherit;" class="modal">
    <input id="id_edit" name="id" type="hidden" value="0">
    <div class="modal-content">
        <div>
            <div style="padding-bottom: 1em;">
                <buttonn class="modal-close" style="color: red;">
                    <i class="material-icons left">close</i>
                    Fechar
                </button>
            </div>
            <div id="loading-edit" class="center-align" style="display: none;">
                <?= $load ?>
            </div>
            <h5>Registrar Cliente</h5>
            <div id="formulario-edit" style="margin-top: 2.5em;">
                <div id="alert-edit"></div>
                <div class="row" style="margin-top: 2em;">
                    <div class="input-field col s6" style="padding-left: 0px;">
                        <input id="name-edit" name="name" placeholder="Nome" type="text" class="validate">
                        <label for="name" style="left: 0px;" >Nome*</label>
                    </div>
                    <div class="input-field col s6" style="padding-right: 0px;">
                        <input id="phone-edit" name="phone" placeholder="Telefone" type="text" class="validate">
                        <label for="phone" style="left: 0px;">Telefone</label>
                    </div>
                </div>
                <h6>Endereço:</h6>
                <br>
                <div class="row">
                    <div class="input-field col s6" style="padding-left: 0px;">
                        <select id="estado-edit" value="0">
                            <option value="0" disabled selected>Selecione o Estado</option>
                            <?php 
                                foreach($estados as $item)
                                {
                                    echo('<option value="'.$item['id'].'">'.$item['name'].'</option>');
                                }
                            ?>
                        </select>
                        <label style="left: 0px;">Estado</label>
                    </div>
                    <div class="input-field col s6" style="padding-right: 0px;">
                        <select id="id_cidade" name="id_cidade" value="0" required>
                            <option value="0" disabled selected>Selecione o Município</option>
                        </select>
                        <label style="left: 0px;">Município</label>
                    </div>
                </div>
                <div class="input-field">
                    <input id="address-edit" name="address" placeholder="Endereço" type="text" class="validate">
                    <label for="address">Endereço</label>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer" style="padding: 0 1.5em;">
        <button type="reset" class="waves-effect waves-light btn" style="background: #E82207;">
            <i class="material-icons left">delete</i>
            Limpar
        </button>
        <button type="submit" class="waves-effect waves-light btn">
            <i class="material-icons left">add</i>
            Registrar
        </button>
    </div>
</form>
<script>
    $(function(){
        $('#formularioEdit').on('submit', function(){
            let data = $(this).serializeArray();

            $('#loading-edit').show('slow');
            $('#formulario-edit').hide('slow');
            $('#alert-edit').hide('slow');
            $('#alert-edti').removeClass('alert danger');
            $('#alert-edti').html('');

            $.ajax({
                url: '<?= SISTEMA["url"]?>clientes/edit',
                method: 'POST',
                data: data,
                success: function(data)
                {
                    loadCliente();
                    $('#alert-edit').addClass('alert');
                    $('#alert-edit').html('Cliente editado com sucesso');
                    $('#formularioRegistro')[0].reset();
                },
                error: function(data)
                {
                    $('#alert-edit').addClass('alert danger');
                    if (data.responseJSON.errors)
                    {
                        for(let index in data.responseJSON.errors)
                        {
                            $('#alert-edit').append(`<div>${data.responseJSON.errors[index]}</div>`);
                        }
                    }
                },
                complete: function()
                {
                    $('#loading-edit').hide('slow');
                    $('#formulario-edit').show('slow');
                    $('#alert-edit').show('slow');
                    setTimeout(function(){
                        $('#alert-edit').removeClass('alert danger');
                        $('#alert-edit').html('');
                    }, 8000)
                }
                
            });
        });
        $('#estado-edit').on('change', function(){
            let id = $(this).val();
            $.ajax({
                url: '<?= SISTEMA["url"] ?>cidades/'+id,
                method: 'get',
                success: function(data)
                {
                    let dados = data.data;

                    $('select[name="id_cidade"]').html('');

                    for(object in dados)
                    {
                        $('select[name="id_cidade"]').append(`
                            <option value="${dados[object].id}" >${dados[object].name}</option>
                        `);
                    }
                    $('select').formSelect();
                },
                error: function(erro)
                {
                    console.log(erro);
                }
            });
        });
    });
</script>