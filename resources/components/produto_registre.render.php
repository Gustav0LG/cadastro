<form id="formularioCreate" class="modal" onsubmit="return false;">
    <div class="modal-content">
        <div style="padding-bottom: 1em;">
            <buttonn class="modal-close" style="color: red;">
                <i class="material-icons left">close</i>
                Fechar
            </button>
        </div>
        <div id="loading" class="center-align" style="display: none;">
            <?= $load ?>
        </div>
        <h5>Registrar Produto</h5>
        <div id="alert"></div>
        <div id="formulario" style="margin-top: 2.5em;">
            <div class="input-field col s6" style="padding-left: 0px;">
                <input id="name" name="name" placeholder="Nome" type="text" class="validate">
                <label for="name" style="left: 0px;">Nome*</label>
            </div>
        </div>
    </div>
    <div class="modal-footer" style="padding: 0 1.5em;">
        <button type="reset" class="waves-effect waves-light btn" style="background: #E82207;">
            <i class="material-icons left">delete</i>
            Limpar
        </button>
        <button type="submit" class="waves-effect waves-light btn">
            <i class="material-icons left">add</i>
            Registrar
        </button>
    </div>
</form>
<script>

    $(function(){
        $('#formularioCreate').on('submit', function(){
            let data = $(this).serializeArray();

            $('#loading').show('slow');
            $('#formulario').hide('slow');
            $('#alert').hide('slow');
            $('#alert').removeClass('alert danger');
            $('#alert').html('');

            $.ajax({
                url: '<?= SISTEMA["url"] ?>produtos/create',
                method: 'POST',
                data: data,
                success: function()
                {
                    $('#alert').addClass('alert');
                    $('#alert').html('Produto cadastrado com sucesso');
                    $('#formularioCreate')[0].reset();
                    loadProdutos();
                },
                error: function(data)
                {
                    $('#alert').addClass('alert danger');
                    if (data.responseJSON.errors)
                    {
                        for(let index in data.responseJSON.errors)
                        {
                            $('#alert').append(`<div>${data.responseJSON.errors[index]}</div>`);
                        }
                    }
                },
                complete: function()
                {
                    $('#loading').hide('slow');
                    $('#formulario').show('slow');
                    $('#alert').show('slow');
                }
            });
        });
    });

</script>
