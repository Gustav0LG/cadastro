<form id="formularioEmpreste" class="modal" onsubmit="return false;">
    <div class="modal-content" style="padding: 4em 2em;">
        <div style="padding-bottom: 1em;">
            <buttonn class="modal-close" style="color: red;">
                <i class="material-icons left">close</i>
                Fechar
            </button>
        </div>
        <div id="loading-empreste" class="center-align" style="display: none;">
            <?= $load ?>
        </div>
        <h5>Emprestar Prdotudo</h5>
        <div id="alert"></div>
        <div id="formulario-empreste" style="margin-top: 2.5em;">
            <input type="hidden" name="id" id="id_produto" value="0">
            <div class="input-field col s6" style="padding-left: 0px;">
                <select id="id_cliente" name="id_cliente" value="0">
                    <option value="0" disabled selected>Selecione o Cliente</option>
                    <?php 
                        foreach($clientes as $item)
                        {
                            echo('<option value="'.$item['id'].'">'.$item['id'].' - '.$item['name'].'</option>');
                        }
                    ?>
                </select>
                <label style="left: 0px;">Estado</label>
            </div>
        </div>
    </div>
    <div class="modal-footer" style="padding: 0 1.5em;">
        <button type="submit" class="waves-effect waves-light btn">
            <i class="material-icons left">add</i>
            Enviar
        </button>
    </div>
</form>
<script>
    $(function(){
        $('#formularioEmpreste').on('submit', function(){
            let data = $(this).serializeArray();

            $.ajax({
                url: '<?= SISTEMA["url"]?>produtos/emprestar',
                data: data,
                method: 'POST',
                success: function(data)
                {
                    console.log(data);
                },
                error: function(error)
                {
                    console.log(error);
                },
                complete: function()
                {
                    loadProdutos();
                }
            });
        });
    });
</script>